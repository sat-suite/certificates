<?php

if (!function_exists('array_get')) {
    /**
     * Get an item from an array using "dot" notation.
     *
     * @param array $array
     * @param  string  $key
     * @param  mixed   $default
     *
     * @return mixed
     */
    function array_get(array $array, $key = null, $default = null) {
        $haystack = $array;

        if (is_null($key)) {
            return $haystack;
        }

        if (isset($haystack[$key])) {
            return $haystack[$key];
        }

        foreach (explode('.', $key) as $segment) {
            if (!is_array($haystack) || !array_key_exists($segment, $haystack)) {
                return $default;
            }

            $haystack = $haystack[$segment];
        }

        return $haystack;
    }
}