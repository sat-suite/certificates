<?php
namespace SatSuite\Certificates\Support;

use SatSuite\Certificates\Exceptions\FileNotReadableException;

class PemParser
{
    /** @var string */
    private $contents;

    public function __construct($contents = null)
    {
        $this->contents = $contents;
    }

    public function load($contents)
    {
        $file = strval(str_replace("\0", '', $contents));

        if (file_exists($file)) {
            if (!is_readable($file)) {
                throw new FileNotReadableException('Private key file is not readable');
            }

            $contents = file_get_contents($file);
        }

        $this->contents = $contents;

        return $this;
    }

    public function getContents()
    {
        return $this->contents;
    }

    public function getCertificate()
    {
        return $this->extractFromBase64('CERTIFICATE');
    }

    public function getPublicKey()
    {
        return $this->extractFromBase64('PUBLIC KEY');
    }

    public function getPrivateKey()
    {
        // See https://github.com/kjur/jsrsasign/wiki/Tutorial-for-PKCS5-and-PKCS8-PEM-private-key-formats-differences
        // PKCS#8 plain private key
        if ($extracted = $this->extractFromBase64('PRIVATE KEY')) {
            return $extracted;
        }

        // PKCS#5 plain private key
        if ($extracted = $this->extractFromBase64('RSA PRIVATE KEY')) {
            return $extracted;
        }

        // PKCS#5 encrypted private key
        if ($extracted = $this->extractRsaProtected()) {
            return $extracted;
        }

        // PKCS#8 encrypted private key
        return $this->extractFromBase64('ENCRYPTED PRIVATE KEY');
    }

    protected function extractFromBase64(string $type)
    {
        $matches = [];
        $type = preg_quote($type, '/');
        $pattern = '/^'
            . '-----BEGIN ' . $type . '-----\r?\n'
            . '([A-Za-z0-9+\/=]+\r?\n)+'
            . '-----END ' . $type . '-----\r?\n?'
            . '$/m';

        preg_match($pattern, $this->getContents(), $matches);

        return $this->normalizeLineEndings(strval(isset($matches[0]) ? $matches[0] : ''));
    }

    protected function extractRsaProtected()
    {
        $matches = [];
        $pattern = '/^'
            . '-----BEGIN RSA PRIVATE KEY-----\r?\n'
            . 'Proc-Type: .+\r?\n'
            . 'DEK-Info: .+\r?\n\r?\n'
            . '([A-Za-z0-9+\/=]+\r?\n)+'
            . '-----END RSA PRIVATE KEY-----\r?\n?'
            . '$/m';

        preg_match($pattern, $this->getContents(), $matches);

        return $this->normalizeLineEndings(strval(isset($matches[0]) ? $matches[0] : ''));
    }

    /**
     * Changes EOL CRLF or LF to PHP_EOL.
     * This won't alter CR that are not at EOL.
     * This won't alter LFCR used in old Mac style
     *
     * @param string $content
     * @return string
     * @internal
     */
    protected function normalizeLineEndings(string $content)
    {
        // move '\r\n' or '\n' to PHP_EOL
        // first substitution '\r\n' -> '\n'
        // second substitution '\n' -> PHP_EOL
        // remove any EOL at the EOF
        return rtrim(str_replace(["\r\n", "\n"], ["\n", PHP_EOL], $content), PHP_EOL);
    }
}
