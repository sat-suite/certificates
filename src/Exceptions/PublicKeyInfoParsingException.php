<?php
namespace SatSuite\Certificates\Exceptions;

class PublicKeyInfoParsingException extends FiscalCredentialsProcessingException
{
}

