<?php
namespace SatSuite\Certificates\Exceptions;

class PrivateKeyFileNotReadableException extends FiscalCredentialsProcessingException
{
}

