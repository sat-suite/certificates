<?php
namespace SatSuite\Certificates\Exceptions;

class FiscalCredentialsMismatchException extends FiscalCredentialsProcessingException
{
}

