<?php
namespace SatSuite\Certificates\Exceptions;

class PrivateKeyInfoParsingException extends FiscalCredentialsProcessingException
{
}

