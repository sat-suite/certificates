<?php
namespace SatSuite\Certificates\Exceptions;

class PublicKeyFileNotReadableException extends FiscalCredentialsProcessingException
{
}

