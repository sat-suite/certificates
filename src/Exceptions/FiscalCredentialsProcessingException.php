<?php
namespace SatSuite\Certificates\Exceptions;

use Exception;

class FiscalCredentialsProcessingException extends Exception
{
}

