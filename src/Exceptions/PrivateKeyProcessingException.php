<?php
namespace SatSuite\Certificates\Exceptions;

class PrivateKeyProcessingException extends FiscalCredentialsProcessingException
{
}

