<?php
namespace SatSuite\Certificates\Exceptions;

class FileNotReadableException extends FiscalCredentialsProcessingException
{
}

