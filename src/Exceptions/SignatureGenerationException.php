<?php
namespace SatSuite\Certificates\Exceptions;

use RuntimeException;

class SignatureGenerationException extends RuntimeException
{
}

