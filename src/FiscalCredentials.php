<?php
namespace SatSuite\Certificates;

use SatSuite\Certificates\Parsers\PublicKeyParser;
use SatSuite\Certificates\Parsers\PrivateKeyParser;
use SatSuite\Certificates\Exceptions\FiscalCredentialsMismatchException;

class FiscalCredentials
{
    private $privateKeyParser;
    public $publicKeyParser;

    private $private;
    public $public;

    public function __construct(PrivateKeyParser $private = null, PublicKeyParser $public = null)
    {
        $this->privateKeyParser = $private ? : new PrivateKeyParser();
        $this->publicKeyParser = $public ? : new PublicKeyParser();
    }

    public function parsers()
    {
        return [$this->privateKeyParser, $this->publicKeyParser];
    }

    public function load($private, $public, $passphrase)
    {
        $this->private = $this->privateKeyParser->load($private, $passphrase);
        $this->public = $this->publicKeyParser->load($public);

        $matches = !!$this->private && !!$this->public
            ? $this->private->matches($this->public)
            : false;

        if (!$matches) {
            throw new FiscalCredentialsMismatchException('Key pairs mismatch');
        }

        return $this;
    }

    public function private()
    {
        return $this->private;
    }

    public function public()
    {
        return $this->public;
    }

    public function sign($data, $algorithm = OPENSSL_ALGO_SHA256)
    {
        return $this->private()->sign($data, $algorithm);
    }

    public function verify($data, $signature, $algorithm = OPENSSL_ALGO_SHA256)
    {
        return $this->public()->verify($data, $signature, $algorithm);
    }
}