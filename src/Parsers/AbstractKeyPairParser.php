<?php
namespace SatSuite\Certificates\Parsers;

use SatSuite\Certificates\Support\PemParser;

abstract class AbstractKeyPairParser
{
    protected $pem;

    public function __construct(PemParser $pem = null)
    {
        $this->pem = $pem ? : new PemParser();
    }
}
