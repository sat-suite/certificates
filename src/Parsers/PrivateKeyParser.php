<?php
namespace SatSuite\Certificates\Parsers;

use SatSuite\Certificates\KeyPairs\PrivateKey;
use SatSuite\Certificates\Exceptions\PrivateKeyFileNotReadableException;

class PrivateKeyParser extends AbstractKeyPairParser
{
    protected $private;

    public function load($private, $passphrase = null)
    {
        $contents = $this->parse($private, $passphrase);

        if ($contents) {
            $private = new PrivateKey($contents, $passphrase);

            $this->private = $private;
        }

        return $this->private;
    }

    protected function parse($private, $passphrase = null)
    {
        $file = strval(str_replace("\0", '', $private));

        if (file_exists($file)) {
            if (!is_readable($file)) {
                throw new PrivateKeyFileNotReadableException('Private key file is not readable');
            }

            $private = file_get_contents($private);
        }

        $contents = $this->pem->load($private)->getPrivateKey();

        if (!$contents) {
            // It could be a DER content, convert to PEM
            $contents = $this->der2pem($private, !empty($passphrase));
        }

        return $contents;
    }

    /**
     * Convert PKCS#8 DER to PKCS#8 PEM
     *
     * @param string $contents can be a PKCS#8 DER
     * @param bool $isEncrypted
     * @return string
     */
    public static function der2pem($contents, $isEncrypted)
    {
        $privateKeyName = ($isEncrypted) ? 'ENCRYPTED PRIVATE KEY' : 'PRIVATE KEY';

        return "-----BEGIN $privateKeyName-----" . PHP_EOL
            . chunk_split(base64_encode($contents), 64, PHP_EOL)
            . "-----END $privateKeyName-----";
    }

}
