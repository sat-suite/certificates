<?php
namespace SatSuite\Certificates\Parsers;

use SatSuite\Certificates\KeyPairs\PublicKey;
use SatSuite\Certificates\Exceptions\PublicKeyFileNotReadableException;

class PublicKeyParser extends AbstractKeyPairParser
{
    protected $public;

    public function load($public)
    {
        $contents = $this->parse($public);

        if ($contents) {
            $public = new PublicKey($contents);

            $this->public = $public;
        }

        return $this->public;
    }

    protected function parse($public)
    {
        $file = strval(str_replace("\0", '', $public));

        if (file_exists($file)) {
            if (!is_readable($file)) {
                throw new PublicKeyFileNotReadableException('Private key file is not readable');
            }

            $public = file_get_contents($public);
        }

        $contents = $this->pem->load($public)->getCertificate();

        if (!$contents) {
            // It could be a DER content, convert to PEM
            $contents = $this->der2pem($public);
        }

        return $contents;
    }

    /**
     * Convert X.509 DER base64 or X.509 DER to X.509 PEM
     *
     * @param string $contents can be a X.509 DER or X.509 DER base64
     * @return string
     */
    public static function der2pem($contents)
    {
        // Effectivelly compare that all the content is base64, if it isn't then encode it
        if ($contents !== base64_encode(base64_decode($contents, true) ?: '')) {
            $contents = base64_encode($contents);
        }

        return '-----BEGIN CERTIFICATE-----' . PHP_EOL
            . chunk_split($contents, 64, PHP_EOL)
            . '-----END CERTIFICATE-----';
    }
}
