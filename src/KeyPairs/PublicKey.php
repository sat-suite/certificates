<?php
namespace SatSuite\Certificates\KeyPairs;

use DateTimeZone;
use DateTimeImmutable;
use SatSuite\Certificates\Exceptions\PublicKeyInfoParsingException;
use SatSuite\Certificates\Exceptions\PublicKeyFileNotReadableException;

class PublicKey
{
    /**
     * Holds original certificate data
     *
     * @var array
     */
    protected $data;

    /**
     * Certificate content or path
     *
     * @var string
     */
    protected $contents;

    /**
     * Constructor
     *
     * @param string $public
     * @param Shell|null $shell
     *
     * @return void
     */
    public function __construct($public = null)
    {
        if ($public) {
            $this->load($public);
        }
    }

    /**
     * Loads info into class properties from public certificate file
     *
     * @param string $public
     *
     * @return CertificateParser
     */
    public function load($public)
    {
        $file = strval(str_replace("\0", '', $public));

        if (file_exists($file)) {
            if (!is_readable($file)) {
                throw new PublicKeyFileNotReadableException('Private key file is not readable');
            }

            $public = file_get_contents($public);
        }

        $this->contents = $public;

        $info = openssl_x509_parse($this->getContents());

        if ($info === false) {
            throw new PublicKeyInfoParsingException('Unable to read public key file');
        }

        $this->data = $info;

        return $this;
    }

    /**
     * Returns original certificate info
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Gets the business name
     *
     * @return string
     */
    public function getRazonSocial()
    {
        return array_get($this->data, 'subject.name');
    }

    /**
     * Get CURP
     *
     * @return string
     */
    public function getCurp()
    {
        $curp = array_get($this->data, 'subject.serialNumber');

        $values = explode(' / ', $curp);

        $values = array_filter($values);

        return count($values) ? array_shift($values) : null;
    }

    /**
     * Gets the serial number for certificate
     *
     * @return string
     */
    public function getSerialNumber()
    {
        $serialNumber = '';
        $number = $this->serialNumber;

        // Convert to hex
        $serial = gmp_strval($number, 16);

        // Extract pairs to get serial number
        for ($i = 1; $i <= strlen($serial); $i++) {
            if ($i % 2 == 0) {
                $serialNumber .= substr($serial, $i - 1, 1 );
            }
        }

        return $serialNumber;
    }

    /**
     * Gets the tax id
     *
     * @return string|null
     */
    public function getRfc()
    {
        $values = explode(' / ', array_get($this->data, 'subject.x500UniqueIdentifier'));

        return count($values) ? $values[0] : null;
    }

    /**
     * Gets the issued at time
     *
     * @return string
     */
    public function getIssuedAt()
    {
        $date = new DateTimeImmutable('@'.((int) array_get($this->data, 'validFrom_time_t')), new DateTimeZone('utc'));

        return $date;
    }

    /**
     * Gets the expiration time
     *
     * @return string
     */
    public function getExpiresAt()
    {
        return new DateTimeImmutable('@'.((int) array_get($this->data, 'validTo_time_t')), new DateTimeZone('utc'));
    }

    public function isExpired()
    {
        return $this->getExpiresAt() < new DateTimeImmutable('now', new DateTimeZone('utc'));
    }

    /**
     * Get cfdi usage
     *
     * @return boolean
     */
    public function getUsage()
    {
        return array_get($this->data, 'extensions.keyUsage');
    }

    /**
     * Get the public certificate
     *
     * @return string
     */
    public function getCertificate($enclosing = false)
    {
        return $this->extract('BEGIN CERTIFICATE', 'END CERTIFICATE', $enclosing);
    }

    /**
     * Get the public key
     *
     * @return string
     */
    public function getPublicKey($enclosing = false)
    {
        return $this->extract('BEGIN PUBLIC KEY', 'END PUBLIC KEY', $enclosing);
    }

    public function isFiel()
    {
        return $this->getUsage() === 'Digital Signature, Non Repudiation, Data Encipherment, Key Agreement';
    }

    public function isCsd()
    {
        return $this->getUsage() === 'Digital Signature, Non Repudiation';
    }

    /**
     * Extract content from PEM certificate file
     *
     * @param  string $initial
     * @param  string $closer
     * @param  string $enclosing
     * @return string
     */
    protected function extract($initial, $closer, $enclosing)
    {
        $content = '';
        $data = explode("\n", $this->contents);
        $load = false;

        foreach ($data as $text) {
            $include = false;

            if (strstr($text, $initial)) {
                $load = true;

                if (!$enclosing) {
                    continue;
                }
            } else if (strstr($text, $closer)) {
                $load = false;

                if ($enclosing) {
                     $include = true;
                }
            }

            if ($load || $include) {
                $content .= trim($text);
            }
        }

        return $content;
    }

    public function getContents()
    {
        return $this->contents;
    }

    public function verify($data, $signature, $algorithm = OPENSSL_ALGO_SHA256)
    {
        return openssl_verify($data, $signature, $this->getContents(), $algorithm);
    }
    public function __get($name)
    {
        return array_get((array)$this->data, $name);
    }

    public function __toString()
    {
        return $this->getContents();
    }
}
