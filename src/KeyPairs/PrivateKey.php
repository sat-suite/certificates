<?php
namespace SatSuite\Certificates\KeyPairs;

use SatSuite\Certificates\Exceptions\SignatureGenerationException;
use SatSuite\Certificates\Exceptions\PrivateKeyInfoParsingException;
use SatSuite\Certificates\Exceptions\PrivateKeyFileNotReadableException;
use SatSuite\Certificates\Exceptions\PrivateKeyProcessingException;

class PrivateKey
{
    private $contents;
    private $passphrase;

    protected $shell;

    public function __construct($private = null, $passphrase = null)
    {
        $this->passphrase = $passphrase;

        if (!!$private) {
            $this->load($private, $passphrase);
        }
    }

    public function load($private, $passphrase = null)
    {
        $file = strval(str_replace("\0", '', $private));

        if (file_exists($file)) {
            if (!is_readable($file)) {
                throw new PrivateKeyFileNotReadableException('Private key file is not readable');
            }

            $private = file_get_contents($private);
        }

        $this->passphrase = $passphrase;
        $this->contents = $private;

        /** @var \OpenSSLAsymmetricKey|false */
        $privateKey = openssl_get_privatekey($this->getContents(), $passphrase);

        if ($privateKey === false) {
            throw new PrivateKeyProcessingException('Unable to process private key');
        }

        try {
            $data = openssl_pkey_get_details($privateKey);

            if ($data === false) {
                throw new PrivateKeyInfoParsingException('Unable to process private key');
            }

            $this->data = $data;
        } finally {
            if (function_exists('openssl_free_key')) {
                openssl_free_key($privateKey);
            }
        }
    }

    public function matches(PublicKey $public)
    {
        /** @var \OpenSSLAsymmetricKey|false */
        $privateKey = openssl_get_privatekey($this->getContents(), $this->getPassphrase());

        $match = false;

        try {
            $match = openssl_x509_check_private_key($public->getContents(), $privateKey);
        } finally {
            if (function_exists('openssl_free_key')) {
                openssl_free_key($privateKey);
            }
        }

        return $match;
    }

    public function sign($data, $algorithm = OPENSSL_ALGO_SHA256)
    {
        $signature = null;
        /** @var \OpenSSLAsymmetricKey|false */
        $privateKey = openssl_get_privatekey($this->getContents(), $this->getPassphrase());

        try {
            openssl_sign($data, $signature, $privateKey, $algorithm);

            if (!$signature) {
                throw new SignatureGenerationException("Can't generate the signature: " . openssl_error_string());
            }
        } finally {
            if (function_exists('openssl_free_key')) {
                openssl_free_key($privateKey);
            }
        }

        return $signature;
    }

    public function getContents()
    {
        return $this->contents;
    }

    public function getPassphrase()
    {
        return $this->passphrase;
    }

    public function __toString()
    {
        return $this->contents;
    }

}
